import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Armazenamento } from '../domain/armazenamento';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private login: string;
  private password: string;
  constructor(public http: HttpClient, private storage: Armazenamento) { }

  logar() {
      const body = {
        login: this.login,
        password: this.password
      }
      

      this.http.post('https://example-ecommerce.herokuapp.com/user/login', body, {responseType:"text"}).subscribe(
        (token: any) => {
        this.storage.salvarToken(this.login, token);
        console.log(token);
      },(error)=> {
        console.log("Erro: " + error)
      })



  }

  ngOnInit() {
  }

}
