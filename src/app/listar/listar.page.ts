import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { StringDecoder } from 'string_decoder';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.page.html',
  styleUrls: ['./listar.page.scss'],
})
export class ListarPage implements OnInit {
  private _id: string;

  public produtos:any;
  constructor( public navCtrl: NavController, public http: HttpClient) { 
    this.listarProdutos()
    
  }
  
  deleteProduto(id: string){
    this.http.delete('http://localhost:3000/produto/'+ id).subscribe();
  }
  
  listarProdutos(){
    let data:Observable<any>;
    data = this.http.get('http://localhost:3000/produto/produtos');
    data.subscribe(resultado => {
      this.produtos = resultado;
  });
    
  


    
    }
    
    ngOnInit() {
    }

}
