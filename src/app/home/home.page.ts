import { Component } from '@angular/core';
import { UrlTree, Router } from '@angular/router';
import { Armazenamento } from '../domain/armazenamento'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private router : Router, private storage: Armazenamento) {
    // if(this.storage.resgatarToken){
    //   this.router.navigate(['listar'])
    // }
  }

  irCadastro(): void{
    this.router.navigate(['cadastro'])
  }
  irLista(): void{
    this.router.navigate(['listar'])
  }


  
}
