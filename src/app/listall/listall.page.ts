import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-listall',
  templateUrl: './listall.page.html',
  styleUrls: ['./listall.page.scss'],
})
export class ListallPage implements OnInit {

  constructor( public navCtrl: NavController, public http: HttpClient) { }

  public clientes: any;

  listarProdutos(){
    let data:Observable<any>;
    data = this.http.get('http://localhost:3000/produto');
    data.subscribe(resultado => {
      this.clientes = resultado;
    })
  }




  ngOnInit() {
  }

}
