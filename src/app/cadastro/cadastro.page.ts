import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  private nomeP: string;
  private marca: string;
  private preco: string;
  private quantidade: Int32List;
  private tag: string;

  constructor(private http: HttpClient) { }


  cadastro(){
    const body = {
      nomeP: this.nomeP,
      marca: this.marca,
      preco: this.preco,
      quantidade: this.quantidade,
      tag: this.tag
    }

    this.http.post('http://localhost:3000/produto/create', body, {responseType: "text"}).subscribe(
      (resultado: any) => {
        console.log(resultado);
      },(erro) => {
        console.log(erro);
      }
      )
    
  }

  ngOnInit() {
  }

}
